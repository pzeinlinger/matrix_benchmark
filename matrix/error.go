package matrix

import "fmt"

const (
	//The matrix returned was nil.
	errorNilMatrix = iota + 1
	//The dimensions of the inputs do not make sense for this operation.
	errorDimensionMismatch
)

type error_ int

func (e error_) Error() string {
	switch e {
	case errorDimensionMismatch:
		return "Input dimensions do not match"
	}
	return fmt.Sprintf("Unknown error code %d", e)
}
func (e error_) String() string {
	return e.Error()
}

var (
	//The dimensions of the inputs do not make sense for this operation.
	ErrorDimensionMismatch = error_(errorDimensionMismatch)
)
