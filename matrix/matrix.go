package matrix

import (
	"fmt"
	"math/rand"
	"time"
)

/*
FlatMatrix representes a matrix backed a an array of flat data
*/
type FlatMatrix struct {
	rows int
	cols int
	// flat matrix data. elements[i*step+j] is row i, col j
	elements []int64
	// actual offset between rows
	step int
}

/*
MakeFlatMatrix returns a new FlatMatrix with the provided elements
*/
func MakeFlatMatrix(elements []int64, rows, cols int) *FlatMatrix {
	A := new(FlatMatrix)
	A.rows = rows
	A.cols = cols
	A.step = cols
	A.elements = elements
	return A
}

/*
MakeRandomFlatMatrix returns a new FlatMatrix with the given size
*/
func MakeRandomFlatMatrix(rows, cols int) *FlatMatrix {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	elementsLen := rows * cols
	elements := make([]int64, elementsLen)
	for i := 0; i < elementsLen; i++ {
		sign := 1
		if r1.Intn(10) < 4 {
			sign = -1
		}
		elements[i] = int64(sign) * int64(r1.Int31n(10))
	}
	return MakeFlatMatrix(elements, rows, cols)
}

/*
Get the element in the ith row and jth column.
*/
func (A *FlatMatrix) GetElements() []int64 {
	return A.elements
}

/*
NullMatrix returns a new matrix filled with 0
*/
func NullMatrix(rows, cols int) *FlatMatrix {
	A := new(FlatMatrix)
	A.elements = make([]int64, rows*cols)
	A.rows = rows
	A.cols = cols
	A.step = cols
	return A
}

/*
Times represents a standard matrix Multiplication
*/
func (A *FlatMatrix) Times(B *FlatMatrix) (C *FlatMatrix, err error) {
	if A.cols != B.rows {
		err = ErrorDimensionMismatch
		return
	}
	C = NullMatrix(A.rows, B.cols)
	for i := 0; i < A.rows; i++ {
		sums := C.elements[i*C.step : (i+1)*C.step]
		for k, a := range A.elements[i*A.step : i*A.step+A.cols] {
			for j, b := range B.elements[k*B.step : k*B.step+B.cols] {
				sums[j] += a * b
			}
		}
	}
	return
}

/*
String beauty prints a matrix
*/
func (A *FlatMatrix) String() string {
	s := "\n"
	for i := 0; i < len(A.elements); i++ {
		if i%A.step == 0 {
			s += "\n"
		}
		s += fmt.Sprintf(" %v", A.elements[i])
	}
	return s
}
