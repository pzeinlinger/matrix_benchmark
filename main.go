package main

import (
	"flag"
	"fmt"
	"sort"
	"sync"
	"time"

	"gitlab.com/pzeinlinger/matrix_benchmark/matrix"
)

func main() {
	numberOfmatrices := flag.Int("num", 1000, "number of matrices to generate and multiply")
	matrixSize := flag.Int("size", 200, "size of matrices n x n")
	flag.Parse()
	fmt.Println("Matrix Multiplication Multi-Threading Benchmark")
	fmt.Println(fmt.Sprintf("Using %v %vx%v matrices", *numberOfmatrices, *matrixSize, *matrixSize))
	matrices := make([]*matrix.FlatMatrix, *numberOfmatrices)
	for i := 0; i < *numberOfmatrices; i++ {
		matrices[i] = matrix.MakeRandomFlatMatrix(*matrixSize, *matrixSize)
	}
	start := time.Now()
	result := matrices[0]
	for i := 1; i < *numberOfmatrices; i++ {
		result, _ = result.Times(matrices[i])
	}
	t := time.Now()
	elapsed := t.Sub(start)
	fmt.Println("single threaded performance: ", elapsed)
	start = time.Now()
	parallelResult := parallelTimes(matrices)
	t = time.Now()
	elapsed2 := t.Sub(start)
	fmt.Println("multi threaded performance: ", elapsed2)
	fmt.Println(fmt.Sprintf(">>> multi threaded calculation is %v %% faster", (1-float32(elapsed2.Nanoseconds())/float32(elapsed.Nanoseconds()))*100))
	fmt.Println("Are results equal?: ", equal(result.GetElements(), parallelResult.GetElements()))
}

func equal(a, b []int64) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

type parJobResult struct {
	id     int
	matrix *matrix.FlatMatrix
}

func parallelTimes(matrices []*matrix.FlatMatrix) (result *matrix.FlatMatrix) {
	var wg sync.WaitGroup
	state := matrices
	copy(matrices, state)
	for len(state) > 1 {
		tasks := len(state)/2 + len(state)%2
		resultsChan := make(chan parJobResult, tasks)
		i := 0
		for i <= len(state)-2 {
			wg.Add(1)
			go parallelWorker(&wg, i, resultsChan, state[i], state[i+1])
			i += 2
		}
		if i < len(state) {
			wg.Add(1)
			go parallelWorker(&wg, i, resultsChan, state[i], nil)
		}
		wg.Wait()
		close(resultsChan)
		results := make([]parJobResult, tasks)
		state = make([]*matrix.FlatMatrix, tasks)

		for i := 0; i < tasks; i++ {
			results[i] = <-resultsChan
		}
		sort.Slice(results, func(i, j int) bool {
			return results[i].id < results[j].id
		})
		for i := 0; i < len(results); i++ {
			state[i] = results[i].matrix
		}
	}
	return state[0]
}

func parallelWorker(wg *sync.WaitGroup, id int, resultsChan chan parJobResult, A *matrix.FlatMatrix, B *matrix.FlatMatrix) {
	defer wg.Done()
	r := parJobResult{
		id: id,
	}
	if B == nil {
		r.matrix = A

	} else {
		r.matrix, _ = A.Times(B)
	}
	resultsChan <- r
	return
}
